# Inspire interview web

## Requirements
1. node 9.3
1. yarn

## Installation
1. `git clone git@gitlab.com:slythe/inspire-interview-web.git`
1. `cd inspire-interview-api`
1. `cp .env.example .env` (configure)
1. `yarn install`


## Run server
1. `yarn run start` (run js and scss)

### Docker alternative
!! needs **.env** file configuration with **API_URL** !!
1. `docker buid -t inspire/web .` (create docker image)
1. `docker run --rm -p 3000:80 --name web -d inspire/web` (run web container)


