import React from 'react';

class Spinner extends React.Component {
    render() {
        return (
            <div className="lds-dual-ring"/>
        )
    }
}

export default Spinner;