import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                created by Pavel Parma
            </footer>
        );
    }
}

export default Footer;
