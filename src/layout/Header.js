import React, {Component} from 'react';
import {connect} from 'react-redux';

import {NavLink} from "react-router-dom";
import {logout} from "../modules/identity";
import {bindActionCreators} from "redux";

class Header extends Component {
    render() {
        return this.props.authenticated ? (
            <header className="header">
                    <NavLink exact to="/" className='link' activeClassName='active'>
                        <i className="icon fas fa-home"/>
                        <span className="title">Home</span>
                    </NavLink>
                    <NavLink exact to="#" className='link'>
                        <i className="icon fas fa-user"/>
                        <span className="title">{this.props.identity.email}</span>
                    </NavLink>
                    <NavLink exact to="/login/" className='link' onClick={() => this.props.logout()}>
                        <i className="icon fas fa-sign-out-alt"/>
                        <span className="title">Logout</span>
                    </NavLink>
            </header>
            ) : (
            <header className="header">
                <NavLink exact to="/login/" className='link' activeClassName='active'>
                    <i className="icon fas fa-sign-in-alt"/>
                    <span className="title">Login</span>
                </NavLink>
            </header>
        );
    }
}

const  mapStateToProps = store => ({
    authenticated: store.identity.authenticated,
    identity: store.identity.data
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({logout}, dispatch)
);


export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(Header);
