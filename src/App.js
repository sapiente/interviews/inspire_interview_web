import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import './sass/app.css'

import Header from "./layout/Header";
import Main from "./layout/Main";
import Footer from "./layout/Footer";

import Vehicles from "./pages/vehicles";
import Login from "./pages/login";
import {withAuth} from "./utils/auth";

class App extends Component {
    render() {
        return (
            <div className="app">
                <Header/>
                <Main>
                    <Switch>
                        <Route exact path="/login/" component={Login}/>
                        <Route exact path="/" component={withAuth(Vehicles)}/>
                    </Switch>
                </Main>
                <Footer/>
            </div>
        );
    }
}

export default App;
