import React from 'react';

class VehicleTypeSelect extends React.Component {
    render() {
        return (
            <div className="row center">
                <i className="icon-vehicle-type fas fa-car" onClick={() => this.props.select('car')}> Car</i>
                <i className="icon-vehicle-type fas fa-motorcycle" onClick={() => this.props.select('motorcycle')}> Motorcycle</i>
                <i className="icon-vehicle-type fas fa-truck" onClick={() => this.props.select('truck')}> Truck</i>
                <i className="icon-vehicle-type fas fa-bus" onClick={() => this.props.select('minibus')}> Minibus</i>
            </div>
        );
    }
}

export default VehicleTypeSelect;