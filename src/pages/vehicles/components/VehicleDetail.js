import React, {Component} from 'react';

class VehicleTable extends Component {
    render() {
        const keys = {
            car: {driving_range: 'Driving range'},
            truck: {max_weight: 'Max weight [kg]', loading_area: <span>Loading area [m<sup>2</sup>]</span>},
            minibus: {seats: 'Sets'},
            motorcycle: []
        };

        return (
            <div>
                {Object.entries(keys[this.props.vehicle.type]).map(([key, title]) => (
                    <div key={key}>{title}: {this.props.vehicle.generalized[key]}</div>
                ))}
            </div>
        );
    }
}

export default VehicleTable;
