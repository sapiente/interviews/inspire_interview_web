import React, {Component} from 'react';
import Actions from './Actions'
import VehicleDetail from "./VehicleDetail";
import Spinner from "../../../components/Spinner";

class VehicleTable extends Component {
    render() {
        return (
            <table className="table table-hover table-vehicles">
                <thead>
                <tr>
                    <td>Weight [kg]</td>
                    <td>Performance [W]</td>
                    <td>Daily price [$]</td>
                    <td>Driving licence group</td>
                    <td>Type</td>
                    <td>Details</td>
                    <td>Owner</td>
                    <td>Actions</td>
                </tr>
                </thead>
                <tbody>
                {this.props.fetching ? (
                    <tr><td colSpan={8}><Spinner/></td></tr>
                ) : Object.entries(this.props.vehicles).map(([group, vehicles]) => (
                    vehicles.map((vehicle, index) => (
                        <tr key={vehicle.id}>
                            <td>{vehicle.weight}</td>
                            <td>{vehicle.performance}</td>
                            <td>{vehicle.daily_price}</td>
                            <td>{vehicle.driving_licence_group_id}</td>
                            <td>{vehicle.type}</td>
                            <td><VehicleDetail vehicle={vehicle}/></td>
                            <td>{vehicle.owner.email}</td>
                            <td>
                                <Actions deleteVehicle={this.props.deleteVehicle} allowed={this.props.identity.id === vehicle.owner.id} data={{vehicle, group, index}} />
                            </td>
                        </tr>
                    ))
                ))}
                </tbody>
            </table>
        );
    }
}

export default VehicleTable;
