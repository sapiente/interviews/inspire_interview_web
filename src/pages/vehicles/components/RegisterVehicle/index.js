import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import RegisterVehicle from "./RegisterVehicle";
import {fetchDrivingLicenceGroups} from "../../../../modules/drivingLicences";
import {fetchTypes, register as registerMotorcycle} from "../../../../modules/motorcycles";
import {register as registerCar} from "../../../../modules/cars";
import {register as registerMinibus} from "../../../../modules/minibuses";
import {register as registerTruck} from "../../../../modules/trucks";


const  mapStateToProps = store => ({
    drivingLicenceGroups: store.drivingLicences.groups,
    motorcycleTypes: store.motorcycles.types,
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({fetchDrivingLicenceGroups, fetchMotorcycleTypes: fetchTypes, registerCar, registerTruck, registerMinibus, registerMotorcycle}, dispatch)
);


export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(RegisterVehicle);