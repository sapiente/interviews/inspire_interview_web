import React from 'react';
import ReactTooltip from 'react-tooltip'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader,} from 'reactstrap';
import VehicleTypeSelect from "../VehicleTypeSelect";
import CarPartialForm from "../VehicleForm/CarPartialForm";
import TruckPartialForm from "../VehicleForm/TruckPartialForm";
import VehicleForm from "../VehicleForm/VehicleForm";
import MinibusPartialForm from "../VehicleForm/MinibusPartialForm";
import MotorcyclePartialForm from "../VehicleForm/MotorcyclePartialForm";

class RegisterVehicle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            type: null
        };

        this.toggle = this.toggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.selectType = this.selectType.bind(this);
    }

    componentDidMount() {
        this.props.fetchDrivingLicenceGroups();
        this.props.fetchMotorcycleTypes();
    }

    toggle() {
        this.setState({modal: !this.state.modal});
    }

    selectType(type) {
        this.setState({type});
    }

    handleSubmit(values) {
        const handlers = {
            car: this.props.registerCar,
            truck: this.props.registerTruck,
            minibus: this.props.registerMinibus,
            motorcycle: this.props.registerMotorcycle,
        };

        handlers[this.state.type](values).then(this.toggle);
    }

    getInitialState() {
        const initialVehicleState = {
            weight: 0, performance: 0, daily_price: 0,
            driving_licence_group_id: ((this.props.drivingLicenceGroups[0] || {id: null}).id)
        };

        const vehicleTypes = {
            car: {driving_range: 0},
            truck: {max_weight: 0, loading_area: 0},
            minibus: {seats: 0},
            motorcycle: {type: ((this.props.motorcycleTypes[0] || {id: null}).id)}
        };

        return Object.assign({}, initialVehicleState, vehicleTypes[this.state.type] || {});
    }


    render() {

        const partialForms = {
            car: <CarPartialForm/>,
            truck: <TruckPartialForm/>,
            minibus: <MinibusPartialForm/>,
            motorcycle: <MotorcyclePartialForm types={this.props.motorcycleTypes}/>,
        };

        return (
            <div className="form-register-vehicle">
                <Button className="btn-green" color="success" onClick={this.toggle}>Register vehicle</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} onClosed={() => this.selectType(null)}>
                    <ModalHeader toggle={this.toggle}>Register new vehicle</ModalHeader>
                    <ModalBody>
                        {this.state.type === null ? (
                            <VehicleTypeSelect select={this.selectType}/>
                        ) : (
                            <VehicleForm initialState={this.getInitialState()} buttonTitle="Register" handleSubmit={this.handleSubmit} drivingLicenceGroups={this.props.drivingLicenceGroups}>
                                {partialForms[this.state.type] || null}
                            </VehicleForm>
                        )}
                    </ModalBody>
                    <ModalFooter>
                        {this.state.type !== null ? ([
                            <i key="back" className="pointer fas fa-arrow-circle-left" data-tip data-for="back" onClick={() => this.selectType(null)}/>,
                            <ReactTooltip  key="back-tooltip" id='back' aria-haspopup='true'>
                                <span>Back</span>
                            </ReactTooltip>
                        ]) : null}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default RegisterVehicle;
