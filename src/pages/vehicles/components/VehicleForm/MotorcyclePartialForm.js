import React from 'react';
import {Control} from "react-redux-form";

class MotorcyclePartialForm extends React.Component {

    render() {
        return (
            <div className="row form-group">
                <label htmlFor="type" className="col-sm-4 col-form-label">Select</label>
                <div className="col-sm-8">
                    <Control.select  id="type" model=".type" className="form-control" >
                        {this.props.types.map(type => (
                            <option key={type.id} value={type.id}>{type.name}</option>
                        ))}
                    </Control.select>
                </div>
            </div>
        );
    }
}

export default MotorcyclePartialForm;