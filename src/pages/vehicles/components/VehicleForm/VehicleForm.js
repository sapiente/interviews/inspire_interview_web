import React from 'react';
import {Control, LocalForm} from "react-redux-form";

class VehicleForm extends React.Component {
    render() {
        return (
            <LocalForm onSubmit={this.props.handleSubmit}  initialState={this.props.initialState}>
                <div className="row form-group">
                    <label htmlFor="weight" className="col-sm-4 col-form-label">Weight [kg]</label>
                    <div className="col-sm-8">
                        <Control.text type="number" model=".weight" id="weight" className="form-control" />
                    </div>
                </div>
                <div className="row form-group">
                    <label htmlFor="performance" className="col-sm-4 col-form-label">Performance [W]</label>
                    <div className="col-sm-8">
                        <Control.text type="number" id="performance" model=".performance" className="form-control" />
                    </div>
                </div>
                <div className="row form-group">
                    <label htmlFor="daily_price" className="col-sm-4 col-form-label">Daily price [$]</label>
                    <div className="col-sm-8">
                        <Control.text type="number" model=".daily_price" id="daily_price" className="form-control" />
                    </div>
                </div>
                <div className="row form-group">
                    <label htmlFor="driving_licence_group_id" className="col-sm-4 col-form-label">Select</label>
                    <div className="col-sm-8">
                        <Control.select   id="driving_licence_group_id" model=".driving_licence_group_id" className="form-control" >
                            {this.props.drivingLicenceGroups.map(group => (
                                <option key={group.id} value={group.id}>{group.id}</option>
                            ))}
                        </Control.select>
                    </div>
                </div>
                {this.props.children}
                <div className="row form-group right">
                    <div className="col-sm-12">
                        <button className="btn btn-success">{this.props.buttonTitle}</button>
                    </div>
                </div>
            </LocalForm>
        );
    }
}

export default VehicleForm;