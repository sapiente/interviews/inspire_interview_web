import React from 'react';
import {Control} from "react-redux-form";

class TruckPartialForm extends React.Component {

    render() {
        return (
            <div>
                <div className="row form-group">
                    <label htmlFor="loading_area" className="col-sm-4 col-form-label">Loading area [m<sup>2</sup>]</label>
                    <div className="col-sm-8">
                        <Control.text type="number" model=".loading_area" id="loading_area" className="form-control"/>
                    </div>
                </div>
                <div className="row form-group">
                    <label htmlFor="max_weight" className="col-sm-4 col-form-label">Max weight [kg]</label>
                    <div className="col-sm-8">
                        <Control.text type="number" model=".max_weight" id="max_weight" className="form-control"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default TruckPartialForm;