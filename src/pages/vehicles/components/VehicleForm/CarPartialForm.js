import React from 'react';
import {Control} from "react-redux-form";

class CarPartialForm extends React.Component {

    render() {
        return (
            <div className="row form-group">
                <label htmlFor="driving_range" className="col-sm-4 col-form-label">Driving range [km]</label>
                <div className="col-sm-8">
                    <Control.text type="number" model=".driving_range" id="driving_range" className="form-control" />
                </div>
            </div>
        );
    }
}

export default CarPartialForm;