import React from 'react';
import {Control} from "react-redux-form";

class MinibusPartialForm extends React.Component {

    render() {
        return (
            <div className="row form-group">
                <label htmlFor="seats" className="col-sm-4 col-form-label">Seats</label>
                <div className="col-sm-8">
                    <Control.text type="number" model=".seats" id="seats" className="form-control"/>
                </div>
            </div>
        );
    }
}

export default MinibusPartialForm;