import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import UpdateVehicle from "./UpdateVehicle";
import {update as updateTruck} from "../../../../modules/trucks";
import {update as updateCar} from "../../../../modules/cars";
import {update as updateMotorcycle} from "../../../../modules/motorcycles";
import {update as updateMinibus} from "../../../../modules/minibuses";


const  mapStateToProps = store => ({
    drivingLicenceGroups: store.drivingLicences.groups,
    motorcycleTypes: store.motorcycles.types,
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({updateCar, updateTruck, updateMinibus, updateMotorcycle}, dispatch)
);


export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(UpdateVehicle);