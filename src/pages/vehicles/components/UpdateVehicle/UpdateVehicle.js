import React from 'react';
import {Modal, ModalBody, ModalHeader,} from 'reactstrap';
import CarPartialForm from "../VehicleForm/CarPartialForm";
import TruckPartialForm from "../VehicleForm/TruckPartialForm";
import VehicleForm from "../VehicleForm/VehicleForm";
import MinibusPartialForm from "../VehicleForm/MinibusPartialForm";
import MotorcyclePartialForm from "../VehicleForm/MotorcyclePartialForm";

class UpdateVehicle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {modal: false};

        this.toggle = this.toggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.props.setToggle(this.toggle);
    }

    toggle() {
        this.setState({modal: !this.state.modal});
    }

    handleSubmit(values) {
        const handlers = {
            car: this.props.updateCar,
            truck: this.props.updateTruck,
            minibus: this.props.updateMinibus,
            motorcycle: this.props.updateMotorcycle,
        };

        handlers[this.props.vehicle.type](this.props.vehicle.generalized.id, values).then(this.toggle);
    }

    getInitialState() {
        const vehicle = this.props.vehicle;
        const generalized = vehicle.generalized;

        const initialVehicleState = {
            weight: vehicle.weight, performance: vehicle.performance, daily_price: vehicle.daily_price,
            driving_licence_group_id: vehicle.driving_licence_group_id
        };

        const vehicleTypes = {
            car: {driving_range: generalized.driving_range},
            truck: {max_weight: generalized.max_weight, loading_area: generalized.loading_area},
            minibus: {seats: generalized.seats},
            motorcycle: {type: (generalized.type || {id: null}).id}
        };

        return Object.assign({}, initialVehicleState, vehicleTypes[this.props.vehicle.type] || {});
    }

    render() {

        const partialForms = {
            car: <CarPartialForm/>,
            truck: <TruckPartialForm/>,
            minibus: <MinibusPartialForm/>,
            motorcycle: <MotorcyclePartialForm types={this.props.motorcycleTypes}/>,
        };

        return (
            <div className="form-register-vehicle">
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Update vehicle</ModalHeader>
                    <ModalBody>
                        <VehicleForm initialState={this.getInitialState()} buttonTitle="Update" handleSubmit={this.handleSubmit} drivingLicenceGroups={this.props.drivingLicenceGroups}>
                            {partialForms[this.props.vehicle.type] || null}
                        </VehicleForm>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

export default UpdateVehicle;
