import React, {Component} from 'react';
import ReactTooltip from 'react-tooltip'
import UpdateVehicle from "./UpdateVehicle";


class Actions extends Component {
    render() {
        const {vehicle, group, index} = this.props.data;

        return (
            <span>
                {this.props.allowed ? ([
                    <UpdateVehicle vehicle={vehicle} key="edit-modal" setToggle={(toggle) => this.toggle = toggle}/>,
                    <i key="edit" data-tip data-for="edit" className="icon-action fas fa-pencil-alt" onClick={() => this.toggle()}/>,
                    <ReactTooltip key="edit-tooltip" id='edit' aria-haspopup='true'>
                        <span>Edit</span>
                    </ReactTooltip>,
                    <i key="delete" data-tip data-for="delete" className="icon-action fas fa-trash-alt" onClick={() => this.props.deleteVehicle(vehicle.id, group, index)}/>,
                    <ReactTooltip  key="delete-tooltip" id='delete' aria-haspopup='true'>
                        <span>Delete</span>
                    </ReactTooltip>,
                ]): ([
                    <i key="not-allowed" data-tip data-for="not-allowed" className="fas fa-ban"/>,
                    <ReactTooltip  key="not-allowed-tooltip" id='not-allowed' aria-haspopup='true'>
                        <span>No action allowed</span>
                    </ReactTooltip>,
                ])}
            </span>
        );
    }
}

export default Actions;
