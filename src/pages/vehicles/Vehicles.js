import React, {Component} from 'react';
import RegisterVehicle from "./components/RegisterVehicle";
import VehicleTable from "./components/VehicleTable";

class Vehicles extends Component {

    componentDidMount() {
        this.props.fetchVehicles();

        this.sumVehicles = this.sumVehicles.bind(this);
    }

    sumVehicles() {
        return Object.entries(this.props.vehicles).reduce((carry, [group, vehicles]) => carry + vehicles.length, 0)
    }

    render() {
        return (
            <div>
                <div>Number of vehicles: {this.sumVehicles()}</div>
                <RegisterVehicle/>
                <VehicleTable fetching={this.props.fetching} vehicles={this.props.vehicles} deleteVehicle={this.props.deleteVehicle} identity={this.props.identity}/>
            </div>
        );
    }
}

export default Vehicles;
