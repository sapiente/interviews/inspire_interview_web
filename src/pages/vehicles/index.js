import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import Vehicles from "./Vehicles";
import {deleteVehicle, fetchVehicles} from "../../modules/vehicles";

const mapStateToProps = store => ({
    vehicles: store.vehicles.data,
    fetching: store.vehicles.fetching,
    identity: store.identity.data,
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({fetchVehicles, deleteVehicle}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Vehicles);
