import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {LocalForm, Control} from "react-redux-form";


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {errors: false};

        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(values) {
        this.props.login(values.email, values.password)
            .then(() => this.props.fetchIdentity().then(() => this.props.history.push("/")))
            .catch(() => this.setState({errors: true}));
    }

    render() {
        return (
            <LocalForm className="form-login" onSubmit={this.handleSubmit} initialState={{email: '', password: ''}}>
                {this.state.errors ? (
                    <div className="form-group">
                        <i className="error fas fa-exclamation-triangle"> Please check your credentials</i>
                    </div>
                ) : null}
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <Control.text type="email" className="form-control" model=".email" id="email" placeholder="Email"/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <Control.text type="password" className="form-control" model=".password" id="password" placeholder="Password"/>
                </div>
                <div className="row form-group right">
                    <div className="col-sm-12">
                        <button type="submit" className="btn btn-success">Login</button>
                    </div>
                </div>
            </LocalForm>
        );
    }
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    fetchIdentity: PropTypes.func.isRequired
};

export default Login;
