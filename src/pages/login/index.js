import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import Login from "./Login";
import {fetchIdentity, login} from "../../modules/identity";

const mapDispatchToProps = dispatch => (
    bindActionCreators({login, fetchIdentity}, dispatch)
);

export default connect(null, mapDispatchToProps)(Login);
