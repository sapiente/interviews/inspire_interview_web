import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import axios from "axios";
import store from './store';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import {initAuthToken} from "./utils/auth";
import {fetchIdentity} from "./modules/identity";

axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common = {
    "responseType": "json",
    "Content-Type": "application/json",
};

initAuthToken(() => store.dispatch(fetchIdentity()));

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App/>
        </Provider>
    </BrowserRouter>, document.getElementById('root'));