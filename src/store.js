import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import { combineReducers } from 'redux'

import identity from './modules/identity';
import vehicles from "./modules/vehicles";
import drivingLicences from "./modules/drivingLicences";
import motorcycles from "./modules/motorcycles";


const reducer = combineReducers({
    identity, vehicles, drivingLicences, motorcycles
});


const middleware = applyMiddleware(thunk, promise(), createLogger());

export default createStore(reducer, middleware)