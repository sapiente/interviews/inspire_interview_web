import axios from "axios";
import {connect} from "react-redux";
import React from "react";
import PropTypes from 'prop-types';
import Spinner from "../components/Spinner";

const JWT_KEY = 'jwt';

export function setAuthToken(token) {
    localStorage.setItem(JWT_KEY, token);
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

export function initAuthToken(callback) {
    if (localStorage.getItem(JWT_KEY)) {
        setAuthToken(localStorage.jwt);
        callback();
    }
}

export function deleteAuthToken() {
    localStorage.removeItem(JWT_KEY);
    delete axios.defaults.headers.common['Authorization'];
}


export function withAuth(ComposedComponent) {
    class requireAuth extends React.Component {

        render() {
            if (!this.props.authenticated && !this.props.fetching) {
                if (this.props.location.pathname !== "/login") this.props.history.push("/login");
            }
            return this.props.fetching ? <Spinner/> : <ComposedComponent {...this.props}/>
        }
    }

    requireAuth.propTypes = {
        authenticated: PropTypes.bool.isRequired,
        fetching: PropTypes.bool.isRequired
    };

    const mapStateToProps = store => ({
        authenticated: store.identity.authenticated,
        fetching: store.identity.fetching
    });

    return connect(mapStateToProps)(requireAuth)
}