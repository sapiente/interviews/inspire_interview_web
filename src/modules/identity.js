import {deleteAuthToken, setAuthToken} from "../utils/auth";
import axios from "axios";

// Actions
const LOGIN = 'LOGIN';
const FETCH_IDENTITY = 'FETCH_IDENTITY';
const LOGOUT = 'LOGIN';

const initState = {
    authenticated: false,
    fetching: false,
    data: {}
};

// Reducer
export default function reducer(state = initState, action = {}) {
    switch (action.type) {
        case `${FETCH_IDENTITY}_PENDING`: return {
            ...state,
            fetching: true
        };
        case `${FETCH_IDENTITY}_FULFILLED`: return {
            ...state,
            authenticated: true,
            fetching: false,
            data: action.payload
        };
        case LOGOUT: return {
            ...initState,
        };
        default:
            return state;
    }
}

// Action Creators
export function login(email, password) {
    return {
        type: LOGIN,
        payload: axios.post('/api/auth/login', {email, password}).then(response => setAuthToken(response.data.data.token))
    };
}

export function logout() {
    deleteAuthToken();
    return {
        type: LOGOUT
    };
}

export function fetchIdentity() {
    return {
        type: FETCH_IDENTITY,
        payload: axios.get('/api/users/me').then(response => response.data.data.user)
    };
}