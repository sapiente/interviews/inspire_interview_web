import axios from "axios";

// Actions
const FETCH_DRIVING_LICENCE_GROUPS = 'FETCH_DRIVING_LICENCE_GROUPS';

const initState = {
    groups: [],
};

// Reducer
export default function reducer(state = initState, action = {}) {
    switch (action.type) {
        case `${FETCH_DRIVING_LICENCE_GROUPS}_FULFILLED`: return {
            ...state,
            groups: action.payload
        };
        default:
            return state;
    }
}

// Action Creators

export function fetchDrivingLicenceGroups() {
    return {
        type: FETCH_DRIVING_LICENCE_GROUPS,
        payload: axios.get(`/api/driving-licences/groups/`).then(response => response.data.data.groups)
    };
}
