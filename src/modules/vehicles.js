import axios from "axios";

// Actions
const FETCH_VEHICLES = 'FETCH_VEHICLES';
const DELETE_VEHICLE = 'DELETE_VEHICLE';
export const REGISTER_VEHICLE = 'REGISTER_VEHICLE';
export const UPDATE_VEHICLE = 'UPDATE_VEHICLE';

const initState = {
    fetching: false,
    data: {
        cars: [],
        trucks: [],
        minibuses: [],
        motorcycles: [],
    },
};

// Reducer
export default function reducer(state = initState, action = {}) {
    switch (action.type) {
        case `${FETCH_VEHICLES}_PENDING`: return {
            ...state,
            fetching: true
        };
        case `${FETCH_VEHICLES}_FULFILLED`: return {
            ...state,
            fetching: false,
            data: {...state.data, ...action.payload}
        };
        case `${DELETE_VEHICLE}_FULFILLED`: return {
            ...state,
            data: {...state.data, [action.payload.group]: state.data[action.payload.group].filter(vehicle => vehicle.id !== action.payload.id)}
        };
        case `${REGISTER_VEHICLE}_FULFILLED`: return {
            ...state,
            data: {...state.data, [action.payload.group]: state.data[action.payload.group].concat([action.payload.vehicle])}
        };
        case `${UPDATE_VEHICLE}_FULFILLED`: return {
            ...state,
            data: {...state.data, [action.payload.group]: state.data[action.payload.group].filter(vehicle => vehicle.id !== action.payload.vehicle.id).concat([action.payload.vehicle])}
        };
        default:
            return state;
    }
}

// Action Creators
export function fetchVehicles() {
    return {
        type: FETCH_VEHICLES,
        payload: axios.get('/api/vehicles/').then(response => response.data.data)
    };
}

export function deleteVehicle(id, group) {
    return {
        type: DELETE_VEHICLE,
        payload: axios.delete(`/api/vehicles/${id}`).then(() => ({id, group}))
    };
}