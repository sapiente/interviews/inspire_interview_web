import axios from "axios";
import {REGISTER_VEHICLE, UPDATE_VEHICLE} from "./vehicles";

// Actions



// Reducer


// Action creators
export function register(data) {
    return {
        type: REGISTER_VEHICLE,
        payload: axios.post('/api/trucks/', data).then(response => ({group: 'trucks', vehicle: response.data.data.vehicle}))
    };
}

export function update(id, data) {
    return {
        type: UPDATE_VEHICLE,
        payload: axios.put(`/api/trucks/${id}`, data).then(response => ({group: 'trucks', vehicle: response.data.data.vehicle}))
    };
}
