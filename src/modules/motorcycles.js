import axios from "axios";
import {REGISTER_VEHICLE, UPDATE_VEHICLE} from "./vehicles";

// Actions
const FETCH_MOTORCYCLE_TYPES = 'FETCH_MOTORCYCLE_TYPES';

const initState = {
    types: []
};

// Reducer
export default function reducer(state = initState, action = {}) {
    switch (action.type) {
        case `${FETCH_MOTORCYCLE_TYPES}_FULFILLED`: return {
            ...state,
            types: action.payload
        };
        default: return state;
    }
}

// Action Creators
export function fetchTypes() {
    return {
        type: FETCH_MOTORCYCLE_TYPES,
        payload: axios.get(`/api/motorcycles/types/`).then(response => response.data.data.types)
    };
}

export function register(data) {
    return {
        type: REGISTER_VEHICLE,
        payload: axios.post('/api/motorcycles/', data).then(response => ({group: 'motorcycles', vehicle: response.data.data.vehicle}))
    };
}

export function update(id, data) {
    return {
        type: UPDATE_VEHICLE,
        payload: axios.put(`/api/motorcycles/${id}`, data).then(response => ({group: 'motorcycles', vehicle: response.data.data.vehicle}))
    };
}